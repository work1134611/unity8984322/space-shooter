using UnityEngine;

public enum EnemyType{
    Level1,
    Level2,
    Level3,
    Level4,
    Level5
}

[CreateAssetMenu(fileName = "EnemyData", menuName = "ScriptableObjects/EnemyData", order = 1)]
public class EnemyData : ScriptableObject
{
    public int indexInPool;
    public float health;
    public float attackDamage;
    public float speed;
    public float moveDelay;
    public RewardPoints rewards;
    public EnemyType enemyType;
    public Color enemyColor;
}

[System.Serializable]
public class RewardPoints
{
    [Range(0, 50)] public int rewardCoins;
    [Range(0,20)] public int maxGoldReward;
    [Range(0,5)]public int maxEnergyReward;
}
