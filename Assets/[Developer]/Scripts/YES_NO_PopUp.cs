using DG.Tweening;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public static class YES_NO_PopUp
{
    public static void Initialize(GameObject prefab, bool singleAction, string message, Action yes_OK_Callback, Action no_Callback, float duration = 0.2f)
    {
        GameObject clone = GameObject.Instantiate(prefab, UIManager.instance.transform);

        RectTransform rectTransform = clone.GetComponent<RectTransform>();
        rectTransform.localScale = Vector2.zero;
        rectTransform.DOScale(1, duration).SetEase(Ease.OutBounce);

        TextMeshProUGUI textComponent = clone.GetComponentInChildren<TextMeshProUGUI>();
        if (textComponent != null)
        {
            textComponent.text = message;
        }

        Button yesButton = clone.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Button>();
        Button noButton = clone.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Button>();

        if (singleAction)
            noButton.gameObject.SetActive(false);

        yesButton.onClick.AddListener(() =>
        {
            yes_OK_Callback?.Invoke();
            CleanUp(clone, duration);
        });

        noButton.onClick.AddListener(() =>
        {
            no_Callback?.Invoke();
            CleanUp(clone, duration);
        });
    }

    private static void CleanUp(GameObject clone, float duration=0.2f)
    {
        clone.transform.DOScaleY(0.5f, 0.1f).OnComplete(() => { clone.transform.DOScaleX(0f, 0.1f).OnComplete(() => GameObject.Destroy(clone)); });
    }
}
