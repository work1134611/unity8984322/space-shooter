using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Events;

public class CounterText : MonoBehaviour
{
    public UnityEvent EnablePlayer;

    public void OnEnable()
    {
        StartCoroutine(CounterTxt());      
    }

    public IEnumerator CounterTxt()
    {
        TextMeshProUGUI text = GetComponentInChildren<TextMeshProUGUI>();

        yield return new WaitForSeconds(0.2f);
        text.text = "3";

        yield return new WaitForSeconds(0.3f);
        text.text = "2";

        yield return new WaitForSeconds(0.3f);
        text.text = "1";

        yield return new WaitForSeconds(0.3f);

        text.text = "";
        GameManager.onGameStart?.Invoke(NecessaryData.CurrentLevel);
        EnablePlayer.Invoke();
        GameManager.instance.player.GetComponent<SpriteRenderer>().enabled = true;
        gameObject.SetActive(false);
    }
}
