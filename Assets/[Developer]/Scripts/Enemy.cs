using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    public static UnityAction UpdateAbilitiesStatus;

    public int index;
    public float health;
    public float attackDamage;
    public float speed;
    public bool isDead = false;
    public Player player;
    public EnemyData typeOfEnemy;
    SpriteRenderer spriteRenderer;

    private TaskCompletionSource<bool> initializationCompletionSource;

    private void OnEnable()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        initializationCompletionSource = new TaskCompletionSource<bool>();
    }

    private void Start()
    {
        StartCoroutine(WaitAndMove());
    }

    private IEnumerator WaitAndMove()
    {
        // Wait until the initialization is complete
        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => initializationCompletionSource.Task.IsCompleted);

        // Wait for the spawn interval
        yield return new WaitForSeconds(typeOfEnemy.moveDelay);

        // Start moving towards the player
        StartCoroutine(MoveTowardsPlayer());
    }

    private IEnumerator MoveTowardsPlayer()
    {
        while (true)
        {
            if (player != null)
            {
                if (player.isDead)
                {
                    Destroy(this.gameObject);
                    yield break ;
                }
                Vector3 direction = (player.transform.position - transform.position).normalized;
                transform.position += speed * Time.deltaTime * direction;
            }

            yield return null; // Ensure the coroutine yields control back to Unity
        }
    }

    public void Initialize(float _health, float _attackDamage, float _speed, Color _color, EnemyData _typeOfEnemy, int _index)
    {
        this.health = _health;
        this.attackDamage = _attackDamage;
        this.speed = _speed;
        typeOfEnemy = _typeOfEnemy;
        spriteRenderer.color = _color;
        index = _index;
        initializationCompletionSource.SetResult(true);
        name = "Enemy" + index.ToString();
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            isDead = true;
            Destroy(gameObject); // Destroy the enemy if health is zero or less
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player.GetDamage(typeOfEnemy.attackDamage);
            Destroy(gameObject,0.05f);
        }
    }

    private void OnDestroy()
    {
        if (player.currentEnemy == this)
            player.currentEnemy = null;
        AddKillCount();
    }

    public void AddKillCount()
    {
        if (!isDead)        
            return;
        

        GamePlayPanel.instance.UpdateLevelStatus(GameManager.intKilledEnemies);
        GameManager.instance.enemyCount.text = "Kills: " + GameManager.intKilledEnemies++.ToString();
        UpdateScore();
        UpdateAbilitiesStatus?.Invoke();
        //UpdateScore(typeOfEnemy.rewards.maxEnergyReward, typeOfEnemy.rewards.rewardCoins, typeOfEnemy.rewards.maxGoldReward);
    }

    void UpdateScore()
    {
        int energyToAdd = Random.Range(0, typeOfEnemy.rewards.maxEnergyReward);
        int goldToAdd = Random.Range(0, typeOfEnemy.rewards.maxGoldReward);

        NecessaryData.EnergyCount += (Random.Range(0,30) % 2==0 && NecessaryData.EnergyCount <= GameManager.maxEnergy) ?
          energyToAdd: 0;

        if (NecessaryData.EnergyCount > GameManager.maxEnergy)
        {
            energyToAdd = NecessaryData.EnergyCount - GameManager.maxEnergy;
            NecessaryData.EnergyCount = GameManager.maxEnergy;
        }

        NecessaryData.CoinCount += typeOfEnemy.rewards.rewardCoins;
        NecessaryData.GoldCount += Random.Range(0,20) % 10==0 ? goldToAdd : 0 ;

        GameManager.instance.UpdateTexts();

        FloatingTextController.Initialize(GamePlayPanel.instance.messagePrefab, transform.position, "+"+ typeOfEnemy.rewards.rewardCoins.ToString() + " C");
        FloatingTextController.Initialize(GamePlayPanel.instance.messagePrefab, transform.position + new Vector3(0f,0.3f,0), "+" + goldToAdd.ToString() + " G");
        FloatingTextController.Initialize(GamePlayPanel.instance.messagePrefab, transform.position + new Vector3(0,0.6f,0), "+" +energyToAdd.ToString() + " E");
    }
}
