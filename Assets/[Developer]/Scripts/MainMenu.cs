using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Level
{
    Easy,
    Medium,
    Hard
}

public class MainMenu : MonoBehaviour
{
    public static bool gameStarted = false;

    public static void OnClickPlay()
    {

        if(NecessaryData.EnergyCount >= 5)
        {
            gameStarted = true;
            NecessaryData.EnergyCount -= 5;
            GameManager.instance.UpdateTexts();
            UIManager.instance.ShowNextScreenAndHideAllOtherScreen(UIScreens_ENM.gamePlayScreen);
        }
        else
        {
            YES_NO_PopUp.Initialize(GameManager.instance.yesNoPopUpPrefab, true, "Not enough Energy", null,null);
            Debug.Log("Not Enough Energy");
        }
        
    }

    public void OnClickExitGame()
    {
        Debug.Log("Quit");
        YES_NO_PopUp.Initialize(GameManager.instance.yesNoPopUpPrefab,false, "Do you want to QUIT game?", Application.Quit, null);
    }

    public void OnClickMainMenu()
    {
        SceneManager.LoadScene(0);        
    }
}
