using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum Abilities
{
    Damage,
    AttackRate,
    Health,
    SpawnInterval,
    AttackRadius
        //More to be added
}

public class AbilityBtn : MonoBehaviour
{
    public Button button;
    public Abilities ability;
    public TextMeshProUGUI value;
    public TextMeshProUGUI cost;

    [Header("Don't Change Below Values")]
    public string initialValue = "5";
    public string initialCost = "10";

    Player player;

    public void OnClickAbility()
    {
        string msg="";
        int _cost = int.Parse(cost.text);
        if (NecessaryData.CoinCount < _cost)
        {
            Debug.Log("NOT ENOUGH COINS");
                return;
        }
        else
        {
            NecessaryData.CoinCount -= _cost;
            //GameManager.instance.UpdateTexts(0, NecessaryData.CoinCount, 0);

            switch (ability)
            {
                case Abilities.Damage:
                    Debug.Log("Damage Increased");
                    Bullet.damage += 3;
                    value.text = Bullet.damage.ToString();
                    cost.text = "";
                    cost.text = (_cost +2).ToString();
                    msg = "Damage++";
                    break;

                case Abilities.AttackRate:
                    Debug.Log("AttackRate Increased");
                    player.attackRate -= 0.02f;
                    value.text = player.attackRate.ToString();
                    cost.text = "";
                    cost.text = (_cost + 4).ToString();
                    msg = "AttackRate++";
                    break;

                case Abilities.Health:
                    Debug.Log("Health Increased");
                    player.health += 4;
                    value.text = player.health.ToString();
                    cost.text = "";
                    cost.text = (_cost+ 3).ToString();
                    msg = "Health++";
                    FloatingTextController.Initialize(GamePlayPanel.instance.circlePrefab, player.transform.position, msg, 1f, 0.5f);
                    break;

                case Abilities.SpawnInterval:
                    Debug.Log("Enemy Spawn Rate Increased");
                    GameManager.instance.spawnDelay += 0.1f;
                    value.text = GameManager.instance.spawnDelay.ToString();
                    cost.text = "";
                    cost.text += (_cost + 5).ToString();
                    msg = "SpawnInterval++";
                    break;
                case Abilities.AttackRadius:
                    Debug.Log("player attack range Increased");
                    player.attackRadius += 0.01f;
                    value.text = player.attackRadius.ToString();
                    cost.text = "";
                    cost.text += (_cost + 20).ToString();
                    player.attackRadSprite.localScale = 2 * player.attackRadius * Vector3.one;
                    msg = "AttackRadius++";
                    break;
            }

            FloatingTextController.Initialize(GamePlayPanel.instance.messagePrefab, transform.position, msg, 1.5f, 0.5f);
            Enemy.UpdateAbilitiesStatus?.Invoke();
        }
    }

    private void OnEnable()
    {
        Transform parent = transform.parent;

        player = GameManager.instance.player;
        player.AllAbilities = new AbilityBtn[parent.childCount];

        button = GetComponent<Button>();

        for (int i = 0; i < transform.parent.childCount; i++)
        {
            player.AllAbilities[i] = parent.GetChild(i).GetComponent<AbilityBtn>();
        }
    }
}
