using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NecessaryData: MonoBehaviour
{
    #region Getters&Setters

    public static int CurrentLevel
    {
        get => PlayerPrefs.GetInt("currentLevel", 0);
        set
        {
            PlayerPrefs.SetInt("currentLevel", value);
            GameManager.currentLevel = CurrentLevel;
        }
    }

    public static int EnergyCount
    {
        get => PlayerPrefs.GetInt("Energy", 0);
        set
        {
            PlayerPrefs.SetInt("Energy", value);
            GamePlayPanel.intECount = EnergyCount;
        }
    }

    public static int GoldCount
    {
        get => PlayerPrefs.GetInt("Gold", 0);
        set
        {
            PlayerPrefs.SetInt("Gold", value);
            GamePlayPanel.intgCount = GoldCount;
        }
    }

    public static int CoinCount
    {
        get => PlayerPrefs.GetInt("Coin", 0);
        set
        {
            PlayerPrefs.SetInt("Coin", value);
            GamePlayPanel.intCCount = CoinCount;
        }
    }

    //Abilities Stats

    public static float Damage
    {
        get => PlayerPrefs.GetFloat("Damage", 0);
        set
        {
            PlayerPrefs.SetFloat("Damage", value);
            Bullet.damage = Damage;
        }
    }

    public static float AttackRate
    {
        get => PlayerPrefs.GetFloat("AttackRate", 0);
        set
        {
            PlayerPrefs.SetFloat("AttackRate", value);
            GameManager.instance.player.attackRate = AttackRate;
        }
    }

    public static float Health
    {
        get => PlayerPrefs.GetFloat("Health", 0);
        set
        {
            PlayerPrefs.SetFloat("Health", value);
            GameManager.instance.player.health = Health;
        }
    }

    public static float SpawnInterval
    {
        get => PlayerPrefs.GetFloat("SpawnInterval", 0);
        set
        {
            PlayerPrefs.SetFloat("SpawnInterval", value);
            GameManager.instance.spawnDelay = SpawnInterval;
        }
    }

    public static float AttackRadius
    {
        get => PlayerPrefs.GetFloat("AttackRange", 0);
        set
        {
            PlayerPrefs.SetFloat("AttackRange", value);
            GameManager.instance.player.attackRadius = AttackRadius;
        }
    }

    #endregion
}
