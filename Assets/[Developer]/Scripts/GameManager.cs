using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using System.IO;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static System.Action<int> onGameStart;

    public Transform abilitiesParent;
    [SerializeField] Enemy enemyPrefab;
    public GameObject yesNoPopUpPrefab;
    public TextMeshProUGUI enemyCount;
    public static int intKilledEnemies=1;
    public float spawnDelay;
    [SerializeField] float spawnRadius;
    public static int maxEnergy=20;
    public static int currentLevel;
    public Player player;
    [SerializeField] List<LevelData> levelData;
    //public GameObject counterText;

    [Header("Statistics")]
    [SerializeField] TextMeshProUGUI EnergyCountTxt;
    [SerializeField] TextMeshProUGUI coinsCountTxt;
    [SerializeField] TextMeshProUGUI goldCountTxt;

    private static string lastRefillTimeKey = "lastRefillTime";
    static DateTime lastRefillTime;
    int tempCount=0;
    private void Start()
    {
        saveFilePath = Application.dataPath + "/playerData.json";

        if (instance == null)
            instance = this;

        InitializeGameState(LoadGame());
    }

    private void OnEnable()
    {
        intKilledEnemies = 1;
        RefillEnergy();
        onGameStart += Spawn;
        Debug.Log($"Energy: {NecessaryData.EnergyCount} \t Coins: {NecessaryData.CoinCount} \t Gold:  {NecessaryData.GoldCount}");
        //UpdateTexts(NecessaryData.EnergyCount,NecessaryData.CoinCount,NecessaryData.GoldCount);

    }

    private void OnDisable()
    {
        onGameStart -= Spawn;
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.S))
        //    InitializeDataToStore(false);
        //else if (Input.GetKeyDown(KeyCode.R))
        //    InitializeDataToStore(true);
    }

    private void OnApplicationQuit()
    {
        InitializeDataToStore(false);
    }

    void Spawn(int currentLevel)
    {
        StartCoroutine(SpawnEnemiesForCurrentLevel(1));     //-1 at the time of selecting level so don't decrease it here, so start from 1 in argument
        GamePlayPanel.totalEnemes = levelData[currentLevel].GetTotalEnemiesCount();
        Debug.Log("Total Enemies:" + GamePlayPanel.totalEnemes);
    }

    public IEnumerator SpawnEnemiesForCurrentLevel(int levelIndex)
    {
        if (levelIndex < 0 || levelIndex > levelData.Count)
        {
            Debug.LogError("Invalid level index!");
            yield break;
        }

        Debug.Log("Enemies spawning in progress");
        LevelData currentLevelData = levelData[levelIndex-1];
        List<SpawnTask> spawnTasks = new List<SpawnTask>();

        // Create a list of individual enemy spawn tasks
        foreach (var spawnInfo in currentLevelData.enemiesToSpawn)
        {
            for (int i = 0; i < spawnInfo.enemyCount; i++)
            {
                spawnTasks.Add(new SpawnTask { enemyData = spawnInfo.enemyData });
            }
        }

        // Shuffle the list of spawn tasks
        spawnTasks = ShuffleList(spawnTasks);

        foreach (var task in spawnTasks)
        {
            if (player.isDead)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    Destroy(transform.GetChild(i).gameObject);
                }
                player.gameObject.SetActive(false);
                UIManager.instance.ShowNextScreenAndHideAllOtherScreen(UIScreens_ENM.resultScreen);
                GameComplete.OnLevelComplete.Invoke(false, intKilledEnemies-1);
                yield break;
            }
            SpawnEnemy(task.enemyData, spawnRadius);
            tempCount++;
            yield return new WaitForSeconds(spawnDelay);
        }
    }

    private class SpawnTask
    {
        public EnemyData enemyData;
    }

    private void SpawnEnemy(EnemyData enemyData, float spawnRadius)
    {
        if (enemyPrefab == null)
        {
            Debug.LogError("Enemy prefab is not assigned!");
            return;
        }

        // Calculate a random position outside the specified radius
        Vector2 randomDirection = UnityEngine.Random.insideUnitCircle.normalized;
        Vector3 spawnPosition = player.transform.position + (Vector3)(randomDirection * spawnRadius);

        // Instantiate the enemy prefab at the calculated position
        Enemy enemyObject = Instantiate(enemyPrefab, spawnPosition, Quaternion.identity, transform);

        if (enemyObject == null)
        {
            Debug.LogError("The enemy prefab does not have an Enemy component!");
            return;
        }

        // Initialize the enemy
        enemyObject.Initialize(enemyData.health, enemyData.attackDamage, enemyData.speed, enemyData.enemyColor, enemyData, tempCount);  
        //tempCount is only For indexing enemy for name
        enemyObject.player = player; // Assign player to 
    }

    private List<T> ShuffleList<T>(List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            T temp = list[i];
            int randomIndex = UnityEngine.Random.Range(i, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
        return list;
    }

    public void UpdateTexts()
    {
        EnergyCountTxt.text = NecessaryData.EnergyCount.ToString() + "/" + maxEnergy.ToString();
        coinsCountTxt.text = NecessaryData.CoinCount.ToString();
        goldCountTxt.text = NecessaryData.GoldCount.ToString();
    }

    private static void RefillEnergy()
    {
        if (PlayerPrefs.GetString(lastRefillTimeKey,"") == String.Empty)
        {
            Debug.Log("NO refill playerpref fetched: setting up new playerpref");
            PlayerPrefs.SetString(lastRefillTimeKey, DateTime.Now.ToString());
            
        }
        else Debug.Log($"Last Refill TIme: {PlayerPrefs.GetString(lastRefillTimeKey)}");

        lastRefillTime = DateTime.Parse(PlayerPrefs.GetString(lastRefillTimeKey, DateTime.Now.ToString()));
        DateTime currentTime = DateTime.Now;

        TimeSpan timeDifference = currentTime - lastRefillTime;

        int refillIntervalsPassed = (int)timeDifference.Seconds / 15;
        Debug.Log("Last refill time: \t " + lastRefillTime + "\tcurrent Time: \t" + currentTime + "\ttimeDifference in seconds: " + 
            timeDifference.Seconds + "\trefill interval passed: " + refillIntervalsPassed);

        if (refillIntervalsPassed > 0)
        {
            if (timeDifference.Seconds > 1)
            {
                NecessaryData.EnergyCount = maxEnergy;
            }
            else
            {    
                NecessaryData.EnergyCount += 5;
                NecessaryData.EnergyCount = NecessaryData.EnergyCount > maxEnergy ? maxEnergy : NecessaryData.EnergyCount;
            }
            
            PlayerPrefs.SetString(lastRefillTimeKey, DateTime.Now.ToString().ToString());
            Debug.Log("60 seconds have passed so refilling energy\t" + NecessaryData.EnergyCount);
        }
        else
        {
            Debug.Log("60 seconds haven't passed \t" + NecessaryData.EnergyCount);
        }
            PlayerPrefs.Save();
    }

    public void InitializeDataToStore(bool resetAndSave)
    {
        if (resetAndSave)
        {
            NecessaryData.CoinCount = 0;
            NecessaryData.GoldCount = 0;
            NecessaryData.CurrentLevel = 0;
        }

        List<AbilityBtn> AllAbilities = new();
        AllAbilities = player.AllAbilities.ToList();

        PlayerData _pData = new();
        _pData.energy = NecessaryData.EnergyCount;
        _pData.coins = NecessaryData.CoinCount;
        _pData.gold = NecessaryData.GoldCount;
        _pData.abilityData = new AbilityData[AllAbilities.Count];

        Debug.Log(_pData.abilityData.Length);
        for (int i = 0; i < AllAbilities.Count; i++)
        {
            _pData.abilityData[i] = new AbilityData();
            AbilityBtn btn = AllAbilities[i];

            if (resetAndSave)
            {
                btn.value.text = btn.initialValue;
                btn.cost.text = btn.initialCost;
            }

            _pData.abilityData[i].abilityName = btn.name;
            _pData.abilityData[i].cost = btn.cost.text;
            _pData.abilityData[i].value = btn.value.text;
        }

        UpdateTexts();
        SaveGameData(_pData);
    }

    string saveFilePath = "";

    public void SaveGameData(PlayerData gameData)
    {
        // Serialize the game data to JSON
        string json = JsonUtility.ToJson(gameData);

        // Write the JSON to a file
        File.WriteAllText(saveFilePath, json);

        Debug.Log("Game data saved to " + saveFilePath);
    }

    public PlayerData LoadGame()
    {
        if (!File.Exists(saveFilePath))
        {
            Debug.LogError("Save file not found!");
            return null;
        }

        // Read the JSON from the file
        string json = File.ReadAllText(saveFilePath);

        // Deserialize the JSON to a PlayerData object
        PlayerData gameData = JsonUtility.FromJson<PlayerData>(json);

        Debug.Log("Game data loaded from " + saveFilePath);
        return gameData;
    }

    private void InitializeGameState(PlayerData loadedData)
    {
        Debug.Log("Restoring game data");
        // Initialize game state with loaded data
        currentLevel = loadedData.currentLevel;
        //NecessaryData.EnergyCount = loadedData.energy;
        NecessaryData.CoinCount = loadedData.coins;
        NecessaryData.GoldCount = loadedData.gold;

        for (int i = 0; i < loadedData.abilityData.Length; i++)
        {
            AbilityData abilityData = loadedData.abilityData[i];
            AbilityBtn abilityBtn = abilitiesParent.GetChild(i).GetComponent<AbilityBtn>();

            abilityBtn.name = abilityData.abilityName;
            abilityBtn.value.text = abilityData.value;
            abilityBtn.cost.text = abilityData.cost;
        }

        Debug.Log("Updating Texts");
        UpdateTexts();
    }
}

[System.Serializable]
public class PlayerData
{
    public int currentLevel;
    public int energy;
    public int coins;
    public int gold;
    public AbilityData[] abilityData;
}

[System.Serializable]
public class AbilityData
{
    public string abilityName;
    public string value;
    public string cost;
}
