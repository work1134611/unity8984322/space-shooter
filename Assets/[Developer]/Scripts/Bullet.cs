using UnityEngine;

public class Bullet : MonoBehaviour
{
    public static float damage = 5f; // Damage dealt by the bullet
    public Rigidbody2D bulletRB;

    private void OnEnable()
    {
        bulletRB = GetComponent<Rigidbody2D>();
        Destroy(this.gameObject, 4f);   
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Check if the object the bullet collided with is an enemy
        if (other.gameObject.CompareTag("Enemy"))
        {
            // Get the Enemy component and apply damage
            Enemy enemy = other.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(damage);
            }

            // Destroy the bullet after hitting the enemy
            Destroy(gameObject);
        }
    }
}

