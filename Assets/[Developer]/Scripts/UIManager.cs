using System;
using System.Collections.Generic;
using UnityEngine;

public enum UIScreens_ENM
{        
    gamePlayScreen,
    splashScreen,
    MainMenuScreen,
    HomeScreen,
    soundSettingsScreen,
    shopScreen,
    resultScreen
}

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public List<UiScree_Cls> uiScreens;

    private void Awake()
    {
        instance = this;
    }

    public void ShowNextScreenAndHideAllOtherScreen(UIScreens_ENM screenToShow)
    {
        UiScree_Cls scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToShow);
        foreach (var item in uiScreens)
            item.screen.SetActive(false);
        scrn.screen.SetActive(true);
        //LoadingFadeEffect.instance.FadeAll();
    }

    public void ShowNextScreenAndHideCurrentScreen(UIScreens_ENM screenToShow, UIScreens_ENM screenToHide)
    {
        //LoadingFadeEffect.instance.FadeAll();

        UiScree_Cls scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToHide);
        scrn.screen.SetActive(false);

        scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToShow);
        scrn.screen.SetActive(true);
    }

    public void ShowScreen(UIScreens_ENM screenToShow)
    {
        //LoadingFadeEffect.instance.FadeAll();

        UiScree_Cls scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToShow);
        scrn.screen.SetActive(true);
    }

    public void HideScreen(UIScreens_ENM screenToHide)
    {
        //LoadingFadeEffect.instance.FadeAll();

        UiScree_Cls scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToHide);
        scrn.screen.SetActive(false);
    }
}

[Serializable]
public class UiScree_Cls
{
    public string ScreenName;
    public UIScreens_ENM UiScreens_ENM;
    public GameObject screen;
}
