using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameComplete : MonoBehaviour
{

    public static System.Action<bool, int> OnLevelComplete;

    [SerializeField] TextMeshProUGUI resultTxt;
    [SerializeField] TextMeshProUGUI killedEnemiesTxt;
    [SerializeField] TextMeshProUGUI counterTxt;

    private void OnEnable()
    {
        OnLevelComplete += ShowResult;
        StartCoroutine(RunCounter());
    }

    IEnumerator RunCounter()
    {
        yield return new WaitForSeconds(03f);

        counterTxt.text = "Next Level in: " + 1.ToString();
        yield return new WaitForSeconds(1f);

        counterTxt.text = "Next Level in: " + 2.ToString();
        yield return new WaitForSeconds(1f);

        counterTxt.text = "Next Level in: " + 3.ToString();
        yield return new WaitForSeconds(1f);

        AutoLoadNextLevel();
    }

    private void OnDisable()
    {
        OnLevelComplete -= ShowResult;
    }

    private void ShowResult(bool result ,int enemiesKilled)
    {
        if (result)
        {            
            resultTxt.color = Color.green;
            resultTxt.text = " Congratulations You've won!!!";
        }
        else
        {
            resultTxt.color = Color.red;
            resultTxt.text = "Better Luck Next Time";
        }

        killedEnemiesTxt.text = "EnemiesKilled:  " + enemiesKilled.ToString();
    }

    public void OnClickMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    void AutoLoadNextLevel()
    {
        UIManager.instance.ShowNextScreenAndHideAllOtherScreen(UIScreens_ENM.MainMenuScreen);
        MainMenu.OnClickPlay();
        GameManager.onGameStart(NecessaryData.CurrentLevel);
    }
}
