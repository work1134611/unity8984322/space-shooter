using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObjects/LevelData", order = 2)]
public class LevelData : ScriptableObject
{
    public EnemySpawnInfo[] enemiesToSpawn;
    public int GetTotalEnemiesCount() => enemiesToSpawn.Sum(enemy => enemy.enemyCount);
}

    [System.Serializable]
    public class EnemySpawnInfo
    {
        [Range(1,100)]public int enemyCount;
        public EnemyData enemyData;
    }