using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public static class FloatingTextController
{
    private static GameObject textPrefab;
    private static float moveDistance = 100f;
    private static float moveDuration = 1.5f;
    private static Vector3 startpos;
    private static string message;

    public static void Initialize(GameObject _prefab, Vector3 _startPos, string _message ,float _distance= 1.5f, float _duration=0.5f)
    {
        textPrefab = _prefab;
        moveDistance = _distance;
        moveDuration = _duration;
        startpos = _startPos;
        message = _message;

        ShowFloatingText();
    }

    static void ShowFloatingText()
    {
        Debug.Log($"<color=Red> Pop Up used</color>");
        GameObject clone = GameObject.Instantiate(textPrefab, startpos, Quaternion.identity, UIManager.instance.transform);

        if(clone.TryGetComponent(out TextMeshProUGUI t))
        {
        t.text = message;
            t.transform.DOScale(Vector2.one,moveDuration);
            t.transform.DOMoveY(startpos.y + moveDistance, moveDuration).SetEase(Ease.OutQuad)
                .OnComplete(()=>t.transform.DOScale(Vector2.zero, 0.35f)
                .OnComplete(() => GameObject.Destroy(clone))); // Destroy the text after the movement is complete
        }
        else if(clone.TryGetComponent(out Image img))
        {
            img.transform.DOScale(1.05f, 0.2f).OnComplete(()=>GameObject.Destroy(img.gameObject));
        }
    }
}
