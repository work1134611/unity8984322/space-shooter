using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class GamePlayPanel : MonoBehaviour
{
    public static GamePlayPanel instance;

    public GameObject messagePrefab;
    public GameObject circlePrefab;
    [SerializeField] Image lvlProgressImage;
    public static int totalEnemes;
    public static int intECount;
    public static int intCCount;
    public static int intgCount;

    private void Start()
    {
        if (instance == null)
            instance = this;
    }

    private void OnEnable()
    {
        GameManager.onGameStart += StartGame;
    }        

    private void OnDisable()
    {
        GameManager.onGameStart -= StartGame;
    }

    void StartGame(int CurrentLevel)
    {
        GameManager.instance.player.gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);
        lvlProgressImage.fillAmount = 0;
        GameManager.intKilledEnemies = 1;
        // set UI for abilities and other UI and set up ship and it's componenets
    }

    public void UpdateLevelStatus(int enemiesKilled)
    {
        if (totalEnemes > 0)
        {
            lvlProgressImage.fillAmount = (float)enemiesKilled / totalEnemes;
            if (lvlProgressImage.fillAmount == 1)
            {
                GameManager.instance.player.gameObject.SetActive(false);
                UIManager.instance.ShowNextScreenAndHideAllOtherScreen(UIScreens_ENM.resultScreen);
                GameComplete.OnLevelComplete.Invoke(true,enemiesKilled);
                //NecessaryData.CurrentLevel += 1;
            }
        }
    }
}