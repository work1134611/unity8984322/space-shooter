using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject bulletPrefab; // Prefab for the bullets
    public Transform attackRadSprite;
    public Enemy currentEnemy;
    public float bulletSpeed = 5f; // Speed of the bullet
    public float attackRate = 0.7f;
    public float health;
    public float attackRadius;
    public bool isDead;
    public Vector3 direction;

    [SerializeField] Collider2D[] hitColliders;
    public AbilityBtn[] AllAbilities;

    private void OnEnable()
    {
        attackRadSprite.localScale = 2 * attackRadius * Vector3.one;
        StartCoroutine(FindAndShootNearestEnemy());
        Enemy.UpdateAbilitiesStatus += AbilityStatus;
        Debug.Log($"Coin count: {NecessaryData.CoinCount}---------");
        Invoke(nameof(AbilityStatus), 0.3f);
    }

    private void OnDisable()
    {
        Enemy.UpdateAbilitiesStatus -= AbilityStatus;
    }

    private void Update()
    {
        FindNearestEnemy();

        if(Input.GetKey(KeyCode.Space))
        {
            NecessaryData.CoinCount += 50;
            NecessaryData.EnergyCount += 50;
            NecessaryData.GoldCount += 50;
        }

        //if(currentEnemy!=null)
          //  direction = currentEnemy.transform.localPosition - transform.localPosition;
    }

    private IEnumerator FindAndShootNearestEnemy()
    {
        while (true)
        {
            if (currentEnemy != null)
            {
                //Debug.Log("Shoot Enemy");
                ShootEnemy(currentEnemy.transform);
            }
                yield return new WaitForSeconds(attackRate); // Adjust the wait time as needed
        }
    }

    private void FindNearestEnemy()
    {
;        hitColliders = Physics2D.OverlapCircleAll(transform.position, attackRadius,LayerMask.GetMask("Enemy"));
        float closestDistance = Mathf.Infinity;
        Enemy closestEnemy = null;

        foreach (var collider in hitColliders)
        {
                float distance = Vector2.Distance(transform.position, collider.transform.position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestEnemy = collider.GetComponent<Enemy>();
                    currentEnemy = closestEnemy;
                }
        }
    }

    private void ShootEnemy(Transform enemy)
    {
        direction = (enemy.position - transform.position).normalized; // Direction towards the enemy
        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity, transform);
        bullet.GetComponent<Bullet>().bulletRB.AddRelativeForce(bulletSpeed * Time.deltaTime * direction, ForceMode2D.Impulse);
        Debug.DrawLine(transform.position, direction, Color.red, 0.3f);
    }

    public void GetDamage(float Damage)
    {
        Debug.Log("Took damage");        
            health -= Damage;
            FloatingTextController.Initialize(GamePlayPanel.instance.messagePrefab, transform.position, "Health--");
        if (health <= 0)
        {
            isDead = true;
            gameObject.SetActive(false);
        }
    }

    public void AbilityStatus()
    {
        attackRadSprite.localScale = 2 * attackRadius * Vector3.one;
        Debug.Log("parent: " + GameManager.instance.abilitiesParent.name + "\t abilities length: " + AllAbilities.Length);
        List<AbilityBtn> tempAbilities = new(GameManager.instance.abilitiesParent.childCount);

        if (AllAbilities.Length < 1)
            for (int i = 0; i < GameManager.instance.abilitiesParent.childCount; i++)
                tempAbilities.Add(GameManager.instance.abilitiesParent.GetChild(i).GetComponent<AbilityBtn>());
        else
            tempAbilities = AllAbilities.ToList();

        foreach (AbilityBtn abt in tempAbilities)
        {
                Debug.Log($"<color=Red>abt.name: {abt.name} \t interactable: {abt.button.interactable} \t {NecessaryData.CoinCount}</color>");
            if (int.Parse(abt.cost.text) > NecessaryData.CoinCount)
            {
                abt.button.interactable = false;
            }
            else
                abt.button.interactable = true;
            //Debug.Log($"abt: {abt}\t{abt.button.interactable}");
        }
    }
}